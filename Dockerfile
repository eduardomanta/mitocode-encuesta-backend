FROM openjdk:8-jdk-alpine
LABEL maintainer="eduveloper6390@gmail.com"
WORKDIR /workspace
ADD target/encuesta-1.0.jar app.jar

ENV AWS_REGION="us-east-1"
ENV CLIENT_ID="<>"
ENV USER_POOL_ID="<>"
ENV AWS_ACCESS_KEY="<>"
ENV AWS_SECRET_KEY="<>"
ENV HOST="<>"
#ENV HOST="172.16.89.15"
ENV PORT="3306"
#ENV USERNAME="etolentino"
ENV USERNAME="root"
ENV PASSWORD="<>"
ENV DATABASE="encuesta"
#ENV DATABASE="mitocode"
ENV DOCKERIZE_VERSION v0.6.1

#Podemos descargar dockerize o copiarlo desde un directorio
#RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
#    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
#    && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz

ENTRYPOINT exec java -Djava.security.egd=file:/dev/./urandom -jar /workspace/app.jar
EXPOSE 8080
