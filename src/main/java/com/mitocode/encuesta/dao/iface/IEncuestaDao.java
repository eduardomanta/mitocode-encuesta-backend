package com.mitocode.encuesta.dao.iface;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mitocode.encuesta.model.Encuesta;

@Repository
public interface IEncuestaDao  extends JpaRepository<Encuesta, Integer>  {
	@Query(value="select id,nombres,apellidos,edad, case  when curso='j' then 'Java' else 'C Sharp' end  as curso from encuesta",nativeQuery=true)
	public List<Encuesta> list();
}
