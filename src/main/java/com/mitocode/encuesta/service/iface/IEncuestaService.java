package com.mitocode.encuesta.service.iface;

import java.util.List;
import com.mitocode.encuesta.model.Encuesta;

public interface IEncuestaService {
	List<Encuesta> list();
	void save(Encuesta e);
}
