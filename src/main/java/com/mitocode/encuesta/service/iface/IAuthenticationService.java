package com.mitocode.encuesta.service.iface;

import com.mitocode.encuesta.dto.RenewPasswordFirstDTO;
import com.mitocode.encuesta.dto.RespuestaApi;
import com.mitocode.encuesta.dto.UpdatePasswordDTO;

public interface IAuthenticationService {
	public RespuestaApi getToken(String username, String password);
	public RespuestaApi resetNewPasswordFirst(RenewPasswordFirstDTO updatePassword);
	public RespuestaApi updatePassword(UpdatePasswordDTO updatePassword);
	public RespuestaApi signOut(String token);
	public RespuestaApi refreshToken(String token);
}
