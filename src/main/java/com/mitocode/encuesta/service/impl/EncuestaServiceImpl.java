package com.mitocode.encuesta.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.encuesta.dao.iface.IEncuestaDao;
import com.mitocode.encuesta.model.Encuesta;
import com.mitocode.encuesta.service.iface.IEncuestaService;

@Service
public class EncuestaServiceImpl implements IEncuestaService {

	@Autowired
	IEncuestaDao encuestaDao;
	
	@Override
	public List<Encuesta> list() {
		return encuestaDao.list();
	}
	
	@Override
	public void save(Encuesta e) {
		encuestaDao.save(e);
	}

}
