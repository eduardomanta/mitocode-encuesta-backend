package com.mitocode.encuesta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitocode.securityComponent.AwsCognitoJwtAuthenticationFilter;
import com.mitocode.securityComponent.SecurityConfiguration;





@EnableTransactionManagement
@EnableJpaRepositories
@SpringBootApplication(exclude={ SecurityAutoConfiguration.class,ManagementWebSecurityAutoConfiguration.class})
@Import({SwaggerConfig.class, SecurityConfiguration.class})
public class EncuestaApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(EncuestaApplication.class, args);
	}
	
	@Bean
	public AwsCognitoJwtAuthenticationFilter awsCognitoJwtAuthenticationFilter() {
		return new AwsCognitoJwtAuthenticationFilter();
	}
	
	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}
	
	@SuppressWarnings("deprecation")
	@Bean
	public AWSCognitoIdentityProviderClient CognitoClient() {        
        AWSCognitoIdentityProviderClient cognitoClient = new AWSCognitoIdentityProviderClient(new DefaultAWSCredentialsProviderChain());
        cognitoClient.setRegion(Region.getRegion(Regions.US_EAST_1));  
        return cognitoClient;
	}
}

