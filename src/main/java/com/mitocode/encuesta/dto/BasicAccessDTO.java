package com.mitocode.encuesta.dto;

public class BasicAccessDTO {

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
